#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
#EXPOSE 80
EXPOSE 9003

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["Cart-Api/Cart-Api.csproj", "Cart-Api/"]
COPY ["Models/Models.csproj", "Models/"]
COPY ["DAL/DAL.csproj", "DAL/"]
RUN dotnet restore "Cart-Api/Cart-Api.csproj"
COPY . .
WORKDIR "/src/Cart-Api"
RUN dotnet build "Cart-Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Cart-Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Cart-Api.dll"]