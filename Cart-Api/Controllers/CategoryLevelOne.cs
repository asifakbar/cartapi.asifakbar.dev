﻿using DAL.DBContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cart_Api.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class CategoryLevelOneController : ControllerBase
    {
        private readonly CartDBContext _context;
        public CategoryLevelOneController()
        {
            _context = new CartDBContext();
        }
        /// <summary>
        /// Returns complete list of Level 1 Categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IList<CategoryLevel1>> Get()
        {
            return await _context.CategoryLevel1.ToListAsync();
        }
        /// <summary>
        /// Returns single brand based on ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<CategoryLevel1> Get(int id)
        {
            return await _context.CategoryLevel1.FindAsync(id);
        }

    }
}
