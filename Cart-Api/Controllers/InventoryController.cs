﻿using DAL.DBContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cart_Api.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class InventoryController : ControllerBase
    {
        private readonly CartDBContext _context;
        public InventoryController()
        {
            _context = new CartDBContext();
        }
        /// <summary>
        /// Returns complete list of registered brands
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IList<Inventory>> Get()
        {

            var result =
             await _context.Inventories
                .Include(a => a.Product)
                .AsNoTracking()
                .ToListAsync();

            return result;
        }
        /// <summary>
        /// Returns single brand based on ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<Inventory> Get(int id)
        {
            return await _context.Inventories.FindAsync(id);
        }

    }
}
