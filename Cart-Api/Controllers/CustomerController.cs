﻿using DAL.DBContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cart_Api.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly CartDBContext _context;
        public CustomerController()
        {
            _context = new CartDBContext();
        }
        /// <summary>
        /// Return complete list of Customers.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IList<Customer>> Get()
        {
            var result = await _context.Customers
                .Include(a=>a.Orders)
                .AsNoTracking()
                .ToListAsync().ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Return a customer based on Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IQueryable<Customer>> Get(int id)
        {
            var result = await _context.Customers.FindAsync(id);
            return result == null ? null : _context.Customers.Where(a=>a.Id ==id).Include(a=>a.Orders);
        }
    }
}
