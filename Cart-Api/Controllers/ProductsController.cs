﻿using DAL.DBContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cart_Api.Controllers
{
    [Produces("application/json")]
    //[EnableCors(origins:"http://localhost:44332",headers:"*",methods:"*")]
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly CartDBContext _context;
        public ProductsController()
        {
            _context = new CartDBContext();
        }
        /// <summary>
        /// Returns complete list of Products
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<Product>> Get()
        {
            return await _context.Products
                .Include(b=>b.Stock)
                .Include(a => a.ProductDiscounts)
                .AsNoTracking().ToListAsync();
        }

        /// <summary>
        /// Return a particular Product based on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<Product> Get(int id)
        {
            return await _context.Products.FindAsync(id);
        }


    }
}
