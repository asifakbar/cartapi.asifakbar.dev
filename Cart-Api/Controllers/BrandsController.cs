﻿using System.Collections.Generic;

namespace Cart_Api.Controllers
{
    using DAL.DBContext;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Models.Models;
    using System.Threading.Tasks;

    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class BrandsController : ControllerBase
    {
        private readonly CartDBContext _context;
        public BrandsController()
        {
            _context = new CartDBContext();
        }
        /// <summary>
        /// Returns complete list of registered brands
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IList<Brands>> Get()
        {

            var result =
             await _context.Brands
                .Include(a => a.Product)
                .AsNoTracking()
                .ToListAsync() ;

            return result;
        }
        /// <summary>
        /// Returns single brand based on ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<Brands> Get(int id)
        {
            return await _context.Brands.FindAsync(id);
        }

        /// <summary>
        /// Post a new brand.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]

        public async Task<IActionResult> Post([FromBody] Brands value)
        {
            _context.Brands.Add(value);
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Update/Modify a particular brand.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut("{id}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]

        public async Task<IActionResult> Put(int id, [FromQuery] Brands value)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var brandtoupdate=_context.Brands.Find(id);
            if (brandtoupdate == null)
            {
                return NotFound();
            }
            brandtoupdate.BrandName = value.BrandName;
            brandtoupdate.BrandDescrition = value.BrandDescrition;

            _context.Entry(brandtoupdate).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Delete a particular brand
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var brandtodelete = _context.Brands.Find(id);
            if (brandtodelete == null)
            {
                return NotFound();
            }
            _context.Brands.Remove(brandtodelete);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
