﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BrandName = table.Column<string>(type: "TEXT", nullable: true),
                    ParentBrandid = table.Column<int>(type: "INTEGER", nullable: true),
                    BrandDescrition = table.Column<string>(type: "TEXT", maxLength: 125, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryLevel1",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoryName = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryDescription = table.Column<string>(type: "TEXT", maxLength: 125, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryLevel1", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FName = table.Column<string>(type: "TEXT", maxLength: 125, nullable: false),
                    LName = table.Column<string>(type: "TEXT", maxLength: 125, nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: false),
                    PhoneNumber = table.Column<string>(type: "TEXT", nullable: true),
                    PasswordHash = table.Column<string>(type: "TEXT", nullable: false),
                    Country = table.Column<string>(type: "TEXT", nullable: true),
                    State = table.Column<string>(type: "TEXT", nullable: true),
                    City = table.Column<string>(type: "TEXT", nullable: true),
                    StreetAddress = table.Column<string>(type: "TEXT", nullable: true),
                    PostCode = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryLevel2",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoryName = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryDescription = table.Column<string>(type: "TEXT", maxLength: 125, nullable: true),
                    CategoryLevel1Id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryLevel2", x => x.id);
                    table.ForeignKey(
                        name: "FK_CategoryLevel2_CategoryLevel1_CategoryLevel1Id",
                        column: x => x.CategoryLevel1Id,
                        principalTable: "CategoryLevel1",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    OrderDateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CustomerId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryLevel3",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoryName = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryDescription = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryLevel2Id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryLevel3", x => x.id);
                    table.ForeignKey(
                        name: "FK_CategoryLevel3_CategoryLevel2_CategoryLevel2Id",
                        column: x => x.CategoryLevel2Id,
                        principalTable: "CategoryLevel2",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    barcode = table.Column<string>(type: "TEXT", nullable: true),
                    ProductName = table.Column<string>(type: "TEXT", maxLength: 75, nullable: false),
                    ProductPrice = table.Column<float>(type: "REAL", nullable: false),
                    UnitOfMeasurement = table.Column<byte>(type: "INTEGER", nullable: false),
                    ShortDescription = table.Column<string>(type: "TEXT", maxLength: 125, nullable: true),
                    LongDescription = table.Column<string>(type: "TEXT", maxLength: 500, nullable: true),
                    ImagePath = table.Column<string>(type: "TEXT", nullable: true),
                    IsNewProduct = table.Column<bool>(type: "INTEGER", nullable: false),
                    Isfeature = table.Column<bool>(type: "INTEGER", nullable: true),
                    IsSeller = table.Column<bool>(type: "INTEGER", nullable: true),
                    BrandsId = table.Column<int>(type: "INTEGER", nullable: false),
                    CategoryLevel3Id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Brands_BrandsId",
                        column: x => x.BrandsId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_CategoryLevel3_CategoryLevel3Id",
                        column: x => x.CategoryLevel3Id,
                        principalTable: "CategoryLevel3",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Inventories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SKU = table.Column<string>(type: "TEXT", maxLength: 10, nullable: false),
                    UnitsAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    UnitsChanged = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Inventories_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NumberOfUnits = table.Column<int>(type: "INTEGER", nullable: false),
                    ItemTotal = table.Column<float>(type: "REAL", nullable: false),
                    ProductId = table.Column<int>(type: "INTEGER", nullable: false),
                    OrderId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DiscountDescription = table.Column<string>(type: "TEXT", maxLength: 125, nullable: false),
                    SpecialOfferDiscount = table.Column<float>(type: "REAL", nullable: false),
                    ProductId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductDiscounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductDiscounts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Brands",
                columns: new[] { "Id", "BrandDescrition", "BrandName", "ParentBrandid" },
                values: new object[] { 1, "Something", "Food Brand", null });

            migrationBuilder.InsertData(
                table: "Brands",
                columns: new[] { "Id", "BrandDescrition", "BrandName", "ParentBrandid" },
                values: new object[] { 2, "Something", "Baby Brand", null });

            migrationBuilder.InsertData(
                table: "Brands",
                columns: new[] { "Id", "BrandDescrition", "BrandName", "ParentBrandid" },
                values: new object[] { 3, "Something", "Pet brand", null });

            migrationBuilder.InsertData(
                table: "CategoryLevel1",
                columns: new[] { "id", "CategoryDescription", "CategoryName" },
                values: new object[] { 1, null, "Food Cupboard" });

            migrationBuilder.InsertData(
                table: "CategoryLevel1",
                columns: new[] { "id", "CategoryDescription", "CategoryName" },
                values: new object[] { 2, null, "Baby" });

            migrationBuilder.InsertData(
                table: "CategoryLevel1",
                columns: new[] { "id", "CategoryDescription", "CategoryName" },
                values: new object[] { 3, null, "Pets" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "City", "Country", "Email", "FName", "LName", "PasswordHash", "PhoneNumber", "PostCode", "State", "StreetAddress" },
                values: new object[] { 1, null, "Pakistan", "some@something.com", "Some", "One", "5F-4D-CC-3B-5A-A7-65-D6-1D-83-27-DE-B8-82-CF-99", "03214209211", null, null, null });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "City", "Country", "Email", "FName", "LName", "PasswordHash", "PhoneNumber", "PostCode", "State", "StreetAddress" },
                values: new object[] { 2, null, "Pakistan", "some1@something.com", "Some", "Two 205", "0E-76-E3-E1-78-35-5D-9C-38-0B-32-1C-05-B5-B4-BF", "03219211420", null, null, null });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 1, null, 1, "Nuts" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 2, null, 1, "Hot Drinks" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 3, null, 1, "Biscuits & Waffers" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 4, null, 1, "Chocolate" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 5, null, 1, "Sweets & Chewing Gums" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 6, null, 1, "Sweets & Savoury Spreads" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 7, null, 1, "Sweetners" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 8, null, 2, "Baby & Toddler Food" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 9, null, 3, "Cats & Kitten" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 10, null, 3, "Cat Care, Accessories & Toys" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 11, null, 3, "Dog & Puppy" });

            migrationBuilder.InsertData(
                table: "CategoryLevel2",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel1Id", "CategoryName" },
                values: new object[] { 12, null, 3, "Dog Care, Accessories & Toys" });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "CustomerId", "OrderDateTime" },
                values: new object[] { 1, 1, new DateTime(2021, 1, 27, 2, 17, 56, 935, DateTimeKind.Local).AddTicks(5155) });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "CustomerId", "OrderDateTime" },
                values: new object[] { 2, 2, new DateTime(2021, 1, 27, 2, 17, 56, 937, DateTimeKind.Local).AddTicks(407) });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 1, null, 1, "Roasted, Salted & Flavoured Nuts" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 25, null, 12, "Dog Healthcare & Grooming" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 24, null, 11, "Dog Treats & Dental Chews & Biscuit " });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 23, null, 11, "Wet Dog Food" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 22, null, 11, "Dry Dog Food & Mixer" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 21, null, 10, "Cat Toys" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 20, null, 10, "Cat Accesories" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 19, null, 10, "Cat Healthcare & Grooming" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 18, null, 9, "Wet Cat Food" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 17, null, 9, "Cat Treats" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 16, null, 9, "Advance Nutrition Cat Food" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 15, null, 8, "Baby Jars" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 26, null, 12, "Dog Accesories" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 14, null, 7, "Sweetners" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 12, null, 6, "Peanut Butter" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 11, null, 6, "Honey" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 10, null, 5, "Kid Sweets" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 9, null, 5, "Jelly & Chewy Sweets" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 8, null, 5, "Chewing Gum" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 7, null, 4, "Block Chocolate Bars" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 6, null, 3, "Waffers" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 5, null, 3, "Cookies" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 4, null, 2, "Tea" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 3, null, 2, "Coffee" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 2, null, 1, "Dried Fruits" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 13, null, 6, "Choclate & Sweet Spreads" });

            migrationBuilder.InsertData(
                table: "CategoryLevel3",
                columns: new[] { "id", "CategoryDescription", "CategoryLevel2Id", "CategoryName" },
                values: new object[] { 27, null, 12, "Dog Toys" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 1, 1, 1, "ProductImage/1.jpg", false, null, null, null, "Product", 30f, "Some Description", (byte)0, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 2, 1, 3, "ProductImage/2.png", false, null, null, null, "Product-1", 35f, "Some Description 1", (byte)3, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 3, 1, 5, "ProductImage/3.jpg", false, null, null, null, "Product-2", 65f, "Some Description 2", (byte)2, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 4, 2, 7, "ProductImage/1.jpg", false, null, null, null, "Product-3", 90f, "Some Description 3", (byte)1, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 5, 2, 9, "ProductImage/2.png", false, null, null, null, "Product-4", 105f, "Some Description 4", (byte)3, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 6, 2, 11, "ProductImage/3.jpg", false, null, null, null, "Product-5", 145f, "Some Description 5", (byte)2, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 7, 3, 14, "ProductImage/1.jpg", false, null, null, null, "Product-6", 10f, "Some Description 6", (byte)0, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 8, 3, 17, "ProductImage/2.png", false, null, null, null, "Product-7", 5f, "Some Description 7", (byte)1, null });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "BrandsId", "CategoryLevel3Id", "ImagePath", "IsNewProduct", "IsSeller", "Isfeature", "LongDescription", "ProductName", "ProductPrice", "ShortDescription", "UnitOfMeasurement", "barcode" },
                values: new object[] { 9, 3, 21, "ProductImage/3.jpg", false, null, null, null, "Product-8", 325f, "Some Description 8", (byte)3, null });

            migrationBuilder.InsertData(
                table: "Inventories",
                columns: new[] { "Id", "ProductId", "SKU", "UnitsAvailable", "UnitsChanged" },
                values: new object[] { 1, 1, "0001", 1000, 0 });

            migrationBuilder.InsertData(
                table: "Inventories",
                columns: new[] { "Id", "ProductId", "SKU", "UnitsAvailable", "UnitsChanged" },
                values: new object[] { 2, 2, "0002", 5000, 0 });

            migrationBuilder.InsertData(
                table: "Inventories",
                columns: new[] { "Id", "ProductId", "SKU", "UnitsAvailable", "UnitsChanged" },
                values: new object[] { 3, 3, "0003", 450, 0 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 1, 30f, 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 3, 90f, 3, 2, 1 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 2, 70f, 2, 1, 2 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 6, 350f, 10, 2, 2 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 4, 130f, 2, 2, 3 });

            migrationBuilder.InsertData(
                table: "OrderItems",
                columns: new[] { "Id", "ItemTotal", "NumberOfUnits", "OrderId", "ProductId" },
                values: new object[] { 5, 195f, 3, 1, 3 });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryLevel2_CategoryLevel1Id",
                table: "CategoryLevel2",
                column: "CategoryLevel1Id");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryLevel3_CategoryLevel2Id",
                table: "CategoryLevel3",
                column: "CategoryLevel2Id");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_ProductId",
                table: "Inventories",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_OrderId",
                table: "OrderItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_ProductId",
                table: "OrderItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDiscounts_ProductId",
                table: "ProductDiscounts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_BrandsId",
                table: "Products",
                column: "BrandsId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryLevel3Id",
                table: "Products",
                column: "CategoryLevel3Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inventories");

            migrationBuilder.DropTable(
                name: "OrderItems");

            migrationBuilder.DropTable(
                name: "ProductDiscounts");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "CategoryLevel3");

            migrationBuilder.DropTable(
                name: "CategoryLevel2");

            migrationBuilder.DropTable(
                name: "CategoryLevel1");
        }
    }
}
