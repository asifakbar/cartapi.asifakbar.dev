﻿using Microsoft.EntityFrameworkCore;
using Models.Models;
using Models.SupportingModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DBContext
{
    public static class Seed
    {
        public static void DbSeed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brands>().HasData(
                new Brands { Id = 1, BrandName = "Food Brand", BrandDescrition = "Something" },
                new Brands { Id = 2, BrandName = "Baby Brand", BrandDescrition = "Something" },
                new Brands { Id = 3, BrandName = "Pet brand", BrandDescrition = "Something" }
                );
            modelBuilder.Entity<CategoryLevel1>().HasData(
                new CategoryLevel1 { id = 1, CategoryName = "Food Cupboard" },
                new CategoryLevel1 { id = 2, CategoryName = "Baby" },
                new CategoryLevel1 { id = 3, CategoryName = "Pets" }
                );
            modelBuilder.Entity<CategoryLevel2>().HasData(
                new CategoryLevel2 { id = 1, CategoryName = "Nuts", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 2, CategoryName = "Hot Drinks", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 3, CategoryName = "Biscuits & Waffers", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 4, CategoryName = "Chocolate", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 5, CategoryName = "Sweets & Chewing Gums", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 6, CategoryName = "Sweets & Savoury Spreads", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 7, CategoryName = "Sweetners", CategoryLevel1Id = 1 },
                new CategoryLevel2 { id = 8, CategoryName = "Baby & Toddler Food", CategoryLevel1Id = 2 },
                new CategoryLevel2 { id = 9, CategoryName = "Cats & Kitten", CategoryLevel1Id = 3 },
                new CategoryLevel2 { id = 10, CategoryName = "Cat Care, Accessories & Toys", CategoryLevel1Id = 3 },
                new CategoryLevel2 { id = 11, CategoryName = "Dog & Puppy", CategoryLevel1Id = 3 },
                new CategoryLevel2 { id = 12, CategoryName = "Dog Care, Accessories & Toys", CategoryLevel1Id = 3 }
                );
            modelBuilder.Entity<CategoryLevel3>().HasData(
                new CategoryLevel3 { id = 1, CategoryName = "Roasted, Salted & Flavoured Nuts", CategoryLevel2Id = 1 },
                new CategoryLevel3 { id = 2, CategoryName = "Dried Fruits", CategoryLevel2Id = 1 },
                new CategoryLevel3 { id = 3, CategoryName = "Coffee", CategoryLevel2Id = 2 },
                new CategoryLevel3 { id = 4, CategoryName = "Tea", CategoryLevel2Id = 2 },
                new CategoryLevel3 { id = 5, CategoryName = "Cookies", CategoryLevel2Id = 3 },
                new CategoryLevel3 { id = 6, CategoryName = "Waffers", CategoryLevel2Id = 3 },
                new CategoryLevel3 { id = 7, CategoryName = "Block Chocolate Bars", CategoryLevel2Id = 4 },
                new CategoryLevel3 { id = 8, CategoryName = "Chewing Gum", CategoryLevel2Id = 5 },
                new CategoryLevel3 { id = 9, CategoryName = "Jelly & Chewy Sweets", CategoryLevel2Id = 5 },
                new CategoryLevel3 { id = 10, CategoryName = "Kid Sweets", CategoryLevel2Id = 5 },
                new CategoryLevel3 { id = 11, CategoryName = "Honey", CategoryLevel2Id = 6 },
                new CategoryLevel3 { id = 12, CategoryName = "Peanut Butter", CategoryLevel2Id = 6 },
                new CategoryLevel3 { id = 13, CategoryName = "Choclate & Sweet Spreads", CategoryLevel2Id = 6 },
                new CategoryLevel3 { id = 14, CategoryName = "Sweetners", CategoryLevel2Id = 7 },
                new CategoryLevel3 { id = 15, CategoryName = "Baby Jars", CategoryLevel2Id = 8 },
                new CategoryLevel3 { id = 16, CategoryName = "Advance Nutrition Cat Food", CategoryLevel2Id = 9 },
                new CategoryLevel3 { id = 17, CategoryName = "Cat Treats", CategoryLevel2Id = 9 },
                new CategoryLevel3 { id = 18, CategoryName = "Wet Cat Food", CategoryLevel2Id = 9 },
                new CategoryLevel3 { id = 19, CategoryName = "Cat Healthcare & Grooming", CategoryLevel2Id = 10 },
                new CategoryLevel3 { id = 20, CategoryName = "Cat Accesories", CategoryLevel2Id = 10 },
                new CategoryLevel3 { id = 21, CategoryName = "Cat Toys", CategoryLevel2Id = 10 },
                new CategoryLevel3 { id = 22, CategoryName = "Dry Dog Food & Mixer", CategoryLevel2Id = 11 },
                new CategoryLevel3 { id = 23, CategoryName = "Wet Dog Food", CategoryLevel2Id = 11 },
                new CategoryLevel3 { id = 24, CategoryName = "Dog Treats & Dental Chews & Biscuit ", CategoryLevel2Id = 11 },
                new CategoryLevel3 { id = 25, CategoryName = "Dog Healthcare & Grooming", CategoryLevel2Id = 12 },
                new CategoryLevel3 { id = 26, CategoryName = "Dog Accesories", CategoryLevel2Id = 12 },
                new CategoryLevel3 { id = 27, CategoryName = "Dog Toys", CategoryLevel2Id = 12 }
                );
            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, ProductName = "Product", ProductPrice = 30.00F, UnitOfMeasurement = UOM.Kilogram, ShortDescription = "Some Description", ImagePath = "ProductImage/1.jpg", BrandsId = 1, CategoryLevel3Id = 1 },
                new Product { Id = 2, ProductName = "Product-1", ProductPrice = 35.00F, UnitOfMeasurement = UOM.Open, ShortDescription = "Some Description 1", ImagePath = "ProductImage/2.png", BrandsId = 1, CategoryLevel3Id = 3 },
                new Product { Id = 3, ProductName = "Product-2", ProductPrice = 65.00F, UnitOfMeasurement = UOM.Packet, ShortDescription = "Some Description 2", ImagePath = "ProductImage/3.jpg", BrandsId = 1, CategoryLevel3Id = 5 },
                new Product { Id = 4, ProductName = "Product-3", ProductPrice = 90.00F, UnitOfMeasurement = UOM.Grams, ShortDescription = "Some Description 3", ImagePath = "ProductImage/1.jpg", BrandsId = 2, CategoryLevel3Id = 7 },
                new Product { Id = 5, ProductName = "Product-4", ProductPrice = 105.00F, UnitOfMeasurement = UOM.Open, ShortDescription = "Some Description 4", ImagePath = "ProductImage/2.png", BrandsId = 2, CategoryLevel3Id = 9 },
                new Product { Id = 6, ProductName = "Product-5", ProductPrice = 145.00F, UnitOfMeasurement = UOM.Packet, ShortDescription = "Some Description 5", ImagePath = "ProductImage/3.jpg", BrandsId = 2, CategoryLevel3Id = 11 },
                new Product { Id = 7, ProductName = "Product-6", ProductPrice = 10.00F, UnitOfMeasurement = UOM.Kilogram, ShortDescription = "Some Description 6", ImagePath = "ProductImage/1.jpg", BrandsId = 3, CategoryLevel3Id = 14 },
                new Product { Id = 8, ProductName = "Product-7", ProductPrice = 5.00F, UnitOfMeasurement = UOM.Grams, ShortDescription = "Some Description 7", ImagePath = "ProductImage/2.png", BrandsId = 3, CategoryLevel3Id = 17 },
                new Product { Id = 9, ProductName = "Product-8", ProductPrice = 325.00F, UnitOfMeasurement = UOM.Open, ShortDescription = "Some Description 8", ImagePath = "ProductImage/3.jpg", BrandsId = 3, CategoryLevel3Id = 21 }
                );

            modelBuilder.Entity<Inventory>().HasData(
                new Inventory { Id = 1, SKU = "0001", UnitsAvailable = 1000, ProductId = 1 },
                new Inventory { Id = 2, SKU = "0002", UnitsAvailable = 5000, ProductId = 2 },
                new Inventory { Id = 3, SKU = "0003", UnitsAvailable = 450, ProductId = 3 }
                );

            System.Security.Cryptography.MD5 hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] originalBytes = System.Text.ASCIIEncoding.Default.GetBytes("password");
            byte[] encodedBytes = hasher.ComputeHash(originalBytes);
            //var customer = new Customer { Id = 1, Email = "some@something.com", FName = "Some", LName = "One", PhoneNumber = "03214209211", PasswordHash = BitConverter.ToString(encodedBytes) };

            System.Security.Cryptography.MD5 hasher1 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] originalBytes1 = System.Text.ASCIIEncoding.Default.GetBytes("password-1234");
            byte[] encodedBytes1 = hasher1.ComputeHash(originalBytes1);
            //var customer1 = new Customer { Id = 2, Email = "some1@something.com", FName = "Some", LName = "Two 205", PhoneNumber = "03219211420", PasswordHash = BitConverter.ToString(encodedBytes1) };

            modelBuilder.Entity<Customer>().HasData(
                new Customer { Id = 1, Email = "some@something.com", FName = "Some", LName = "One", PhoneNumber = "03214209211", PasswordHash = BitConverter.ToString(encodedBytes) },
                new Customer { Id = 2, Email = "some1@something.com", FName = "Some", LName = "Two 205", PhoneNumber = "03219211420", PasswordHash = BitConverter.ToString(encodedBytes1) }
                );

            modelBuilder.Entity<Order>().HasData(
                new Order { Id = 1, CustomerId = 1, OrderDateTime = DateTime.Now },
                new Order { Id = 2, CustomerId = 2, OrderDateTime = DateTime.Now }
                );
            modelBuilder.Entity<OrderItems>().HasData(
               new OrderItems { Id = 1, ProductId = 1, OrderId = 1, NumberOfUnits = 1, ItemTotal = 30.00F },
                   new OrderItems { Id = 2, ProductId = 2, OrderId = 1, NumberOfUnits = 2, ItemTotal = 70.00F },
                   new OrderItems { Id = 3, ProductId = 1, OrderId = 2, NumberOfUnits = 3, ItemTotal = 90.00F },
                   new OrderItems { Id = 4, ProductId = 3, OrderId = 2, NumberOfUnits = 2, ItemTotal = 130.00F },
                   new OrderItems { Id = 5, ProductId = 3, OrderId = 1, NumberOfUnits = 3, ItemTotal = 195.00F },
                   new OrderItems { Id = 6, ProductId = 2, OrderId = 2, NumberOfUnits = 10, ItemTotal = 350.00F }
               );
        }
    }
}
