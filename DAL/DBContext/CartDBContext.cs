﻿using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.DBContext
{
    public class CartDBContext:DbContext
    {
        public CartDBContext():base()
        {
            //base.Database.Migrate();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.DbSeed();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=blog.db");
        }
        public DbSet<Product> Products { get; set; }

        public DbSet<ProductDiscounts> ProductDiscounts { get; set; }

        public DbSet<Brands> Brands { get; set; }

        public DbSet<CategoryLevel1> CategoryLevel1 { get; set; }
        public DbSet<CategoryLevel2> CategoryLevel2 { get; set; }
        public DbSet<CategoryLevel3> CategoryLevel3 { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
    }
}
