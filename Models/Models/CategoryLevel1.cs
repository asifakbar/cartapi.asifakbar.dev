﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class CategoryLevel1
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }


        public string CategoryName { get; set; }

        [MaxLength(125)]
        public string CategoryDescription { get; set; }

        public virtual ICollection<CategoryLevel2> CategoryLevel2s { get; set; }

    }
}
