﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Inventory
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, MaxLength(10)]
        public string SKU { get; set; }

        [Required, DataType(DataType.Currency)]
        public int UnitsAvailable { get; set; }

        // do a proper Units available and units change table/setting later
        [DataType(DataType.Currency)] //required?
        public int UnitsChanged { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }

    //public class InventoryUnits
    //{

    //}
}
