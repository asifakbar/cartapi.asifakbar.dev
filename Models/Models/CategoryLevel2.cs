﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class CategoryLevel2
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string CategoryName { get; set; }

        [MaxLength(125)]
        public string CategoryDescription { get; set; }

        public int CategoryLevel1Id { get; set; }

        public virtual CategoryLevel1 CategoryLevel1 { get; set; }

        public virtual ICollection<CategoryLevel3> CategoryLevel3s { get; set; }

    }
}
