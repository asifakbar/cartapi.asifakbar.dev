﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ProductDiscounts
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, MaxLength(125)]
        public string DiscountDescription { get; set; }

        [Required, DataType(DataType.Currency)]
        public float SpecialOfferDiscount { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }


    }
}
