﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class OrderItems
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int NumberOfUnits { get; set; }

        [Required]
        public float ItemTotal { get; set; }

        public int ProductId { get; set; }
        public virtual Product Products { get; set; }

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        //public ICollection<Order> Orders { get; set; }
    }
}
