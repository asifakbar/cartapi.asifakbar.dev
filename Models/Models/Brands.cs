﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Models
{
    public class Brands
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string BrandName { get; set; }

        public int? ParentBrandid { get; set; }

        [MaxLength(125)]
        public string BrandDescrition { get; set; }

        //[JsonIgnore]
        public ICollection<Product> Product { get; set; }
    }
}
