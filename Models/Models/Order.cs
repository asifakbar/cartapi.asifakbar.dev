﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Order//ShoppingCart
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, DataType(DataType.DateTime)]
        public DateTime OrderDateTime { get; set; }

        //public int OrderitemId { get; set; }
        //public virtual OrderItems OrderItems { get; set; }

        public ICollection<OrderItems> OrderItems { get; set; }

        //public Customer Customer { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
