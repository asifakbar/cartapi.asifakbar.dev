﻿using Models.SupportingModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Models.Models
{
    public class Product
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string barcode { get; set; }

        [Required, MaxLength(75)]
        public string ProductName { get; set; }

        [Required, DataType(DataType.Currency)]
        public float ProductPrice { get; set; }

        [Required, EnumDataType(typeof(UOM))]
        public UOM UnitOfMeasurement { get; set; }

        [MaxLength(125)]
        public string ShortDescription { get; set; }

        [MaxLength(500)]
        public string LongDescription { get; set; }

        public string ImagePath { get; set; }

        public bool IsNewProduct { get; set; }

        public bool? Isfeature { get; set; }

        public bool? IsSeller { get; set; }

        public int BrandsId { get; set; }

        public virtual Brands Brands { get; set; }

        public int CategoryLevel3Id { get; set; }

        public virtual CategoryLevel3 CategoryLevel3 { get; set; }
        public ICollection<ProductDiscounts> ProductDiscounts { get; set; }

        //public int OrderItemId { get; set; }
        //public OrderItems OrderItems { get; set; }
        public ICollection<OrderItems> OrderItems { get; set; }

        public ICollection<Inventory> Stock { get; set; }
    }
}
