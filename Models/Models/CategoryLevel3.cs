﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class CategoryLevel3
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string CategoryName { get; set; }

        public string CategoryDescription { get; set; }

        public int CategoryLevel2Id { get; set; }

        public virtual CategoryLevel2 CategoryLevel2 { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }

}
