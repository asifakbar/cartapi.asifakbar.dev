﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Customer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, MaxLength(125)]
        public string FName { get; set; }

        [Required, MaxLength(125)]
        public string LName { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required, DataType(DataType.Password)]
        public string PasswordHash { get; set; }

        //[Required]
        public string Country { get; set; } = "Pakistan";

        //[Required]
        public string State { get; set; }

        //[Required]
        public string City { get; set; }

        //[Required]

        public string StreetAddress { get; set; }

        //[Required]
        public string PostCode { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
