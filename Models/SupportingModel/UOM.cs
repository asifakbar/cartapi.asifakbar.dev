﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.SupportingModel
{
    public enum UOM : byte
    {
        Kilogram, Grams, Packet, Open
    }
}
